require 'log_entry'

class LogEntries
  # number of permissable redirects
  REDIRECT_MAX = 5
  # span of time to check redirects
  REDIRECT_WINDOW = 5

  attr_reader :entries
  def initialize(log_file, output_file)
    @log_file, @output_file = log_file, output_file
    @entries = {}
  end

  def add(log_entry)
    @entries[log_entry.ip.to_s] ||= []
    @entries[log_entry.ip.to_s] << log_entry
  end

  def sketch?(ip)
    return false unless entries[ip.to_s]
    redirects = entries[ip.to_s].select{ |entry| entry.redirect? }

    ip.sketch? &&
      redirects.count > REDIRECT_MAX &&
      exceeds_window(redirects.map(&:time))
  end

  def sketch_ips
    File.open(@log_file, 'r').each_line do |line|
      if(entry = LogEntry.parse(line))
        self.add(LogEntry.new(entry))
      end
    end

    sketchy = []
    self.entries.values.map(&:first).each do |entry|
      sketchy << entry.ip.to_s if self.sketch?(entry.ip)
    end
    sketchy
  end

  def exceeds_window(time_arr)
    return false if time_arr.count < REDIRECT_MAX
    return true  if (time_arr[REDIRECT_MAX - 1] - time_arr.first) < REDIRECT_WINDOW
    exceeds_window(time_arr[1..-1])
  end

  def create_blocklist
    new_blocks  = sketch_ips
    curr_blocks = []

    if(File.file?(@output_file))
      curr_blocks += File.read(@output_file).split("\n")
    end

    results = (curr_blocks + new_blocks).uniq.
                reject(&:empty?).
                sort_by { |ip| IP.new(ip).to_binary }
    File.write(@output_file, results.join("\n"))
  end
end
