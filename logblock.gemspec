lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "version"

Gem::Specification.new do |spec|
  spec.name     = "logblock"
  spec.version  = Logblock::VERSION
  spec.authors  = ["Christopher Kuttruff"]
  spec.license  = "BSD-3-Clause"
  spec.summary  = "simple ruby tool for identify sketchy nginx requests"
  spec.homepage = "https://gitlab.com/slackz/log-block"
  spec.files    = ["lib/version.rb", "bin/logblock", "lib/ip.rb", "lib/log_entries.rb", "lib/log_entry.rb"]

  spec.bindir        = "bin"
  spec.executables   = ['logblock']
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler",  "~> 2.0.1"
  spec.add_development_dependency "rake",     "~> 12.3.2"
  spec.add_development_dependency "minitest", "~> 5.11.3"
end
