require 'time'
require 'ip'

class LogEntry
  TIME_FORMAT = '%d/%b/%Y:%H:%M:%S'
  attr_reader :ip, :resp_code, :time

  def initialize(attrs)
    @ip, @resp_code, @time = attrs[:ip], attrs[:resp_code], attrs[:time]
  end

  # Format here is definitely for nginx logs, but easily adaptable
  #   192.168.0.128 - - [25/Jan/2019:06:57:32 +0000] "GET / HTTP/1.1" 200 708 "-" "Awsome Browser"
  def self.parse(line)
    ip_r   = '(\d+\.\d+\.\d+\.\d+)'
    date_r = '\[(.*?)\]'
    str_r  = '"(.*?)"'
    num_r  = '(\d+)'
    regex  = /#{ip_r} - - #{date_r} #{str_r} #{num_r} #{num_r} #{str_r} #{str_r}/

    if(line =~ regex)
      ip, time, req, resp_code, bytes, _, agent = IP.new($1), $2, $3, $4, $5, $6, $7
      time = Time.strptime(time, TIME_FORMAT)

      { ip: ip, resp_code: resp_code, time: time }
    end
  end

  def redirect?
    @resp_code == '301'
  end
end
