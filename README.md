## Log Block

LogBlock is a simple ruby script that parses an nginx access log file and generates a list of potentially abusive IP addresses.  For anyone who has seen spammy entries trying to access phpmyadmin, etc in your web server logs, this tool is for you! :)  I may have this tool support other log formats (and thus other files), but right now the assumption is nginx.

This tool is entirely unobtrusive.  It simply generates a list that can be used for a firewall blocklist - to block all listed IPs at the kernel level with `pf` or `iptables` or something.

LogBlock works by looking for repeated requests within a short period from IPs outside of whitelisted CIDR blocks.  My site setup involves a cloudflare proxy and redirects for any bogus routes, so some elements may need to be tuned for your particular needs

### Dependencies
* ruby / ruby gems (I like to install via [rbenv](https://github.com/rbenv/rbenv))

### Installing / Running
* `gem install logblock`
* `logblock -h`
* See "Example usage" section below for more detailed usage

### TODOs
* allow user to specify CIDR blocks with a config file
* evaluate supportting custom nginx log formats
* etc...

### Example usage
```bash
slackz $ logblock

nginx access log file does not exist: /var/log/nginx/access.log

please run the following for usage: logblock -h

# -----------------------------------------------------------------------------

slackz $ logblock -h

Usage: logblock [options]

    -h, --help                       Show basic usage / options
    -l, --log-file=LOG_FILE          Path to nginx access log
    -o, --output-file=OUTPUT_FILE    Output location for blocklist

# -----------------------------------------------------------------------------

slackz $ logblock -l access.log -o blocklist.txt

blocklist successfully written: blocklist.txt

slackz $ head blocklist.txt
1.214.219.196
1.248.123.42
42.51.27.84
68.51.232.95
77.154.197.227
80.243.110.5
92.87.91.22
93.104.209.19
103.118.197.23
103.224.119.123

```
