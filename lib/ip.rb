class IP
  # -----------------------------------------------------------------------------
  # https://www.cloudflare.com/ips-v4
  EXPECTED_CIDRS = %w(
    103.21.244.0/22  103.22.200.0/22 103.31.4.0/22   131.0.72.0/22    197.234.240.0/22
    173.245.48.0/20  188.114.96.0/20 190.93.240.0/20 108.162.192.0/18 141.101.64.0/18
    198.41.128.0/17  162.158.0.0/15  172.64.0.0/13   104.16.0.0/12
  )
  BITS_IN_IP, BITS_IN_SEGMENT = 32, 8
  attr_reader :bits

  # accepts plain IP address, or CIDR notation
  def initialize(cidr_str)
    @ip, bits = cidr_str.split('/')
    @bits = bits.to_i if bits
  end

  def to_s; @ip; end

  # 192.168.0.1 -> 11000000101010000000000000000001
  def to_binary
    n_to_b = ->(n) { n.to_s(2).rjust(BITS_IN_SEGMENT, '0') }
    @binary_ip ||= @ip.split('.').map { |segment| n_to_b.(segment.to_i) }.join
  end

  def sketch?
    @sketch ||= IP.expected_cidrs.none? { |cidr| cidr.includes?(self) }
  end

  # IP.new('192.168.0.1/24').includes?(IP.new('192.168.0.1'))
  def includes?(ip_addr)
    len = self.bits
    ip_slice = ->(ip) { ip.to_binary[0,len] }
    ip_slice.(self) == ip_slice.(ip_addr)
  end

  def self.expected_cidrs
    @cf_ips ||= EXPECTED_CIDRS.map { |cidr| IP.new(cidr) }
  end
end
